.DEFAULT_GOAL = all

BIN_NAME=mcos
BUILD_DIR=build
BIN_PATH = $(BUILD_DIR)/$(BIN_NAME)

# Call `make V=1` in order to print commands verbosely.
ifeq ($(V),1)
    Q =
else
    Q = @
endif

.PHONY: all
all: build

.PHONY: build
build:
	${Q}CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o $(BIN_PATH) .

.PHONY: run
run: build
	${Q}$(BIN_PATH)

.PHONY: clean
clean:
	${Q}rm -rf $(BUILD_DIR)
