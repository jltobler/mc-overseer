package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
	"strings"
)

func main() {
	log.Println("server starting")

	var addr, dir string
	flag.StringVar(&addr, "address", ":8080", "sets the server address")
	flag.StringVar(&dir, "dir", "", "sets the server directory path")
	flag.Parse()

	if dir == "" {
		log.Println("path to server directory not provided")
		os.Exit(1)
	}

	props, err := os.Open(path.Join(dir, "server.properties"))
	if err != nil {
		log.Printf("opening server properties: %s\n", err)
		os.Exit(1)
	}

	var world string
	scanner := bufio.NewScanner(props)
	for scanner.Scan() {
		line := scanner.Text()
		if value, ok := strings.CutPrefix(line, "level-name="); ok {
			world = value
			break
		}
	}

	if world == "" {
		world = "world"
	}

	packDir := path.Join(dir, world, "datapacks")
	packInfo, err := os.Stat(packDir)
	if err != nil {
		log.Printf("stating datapacks directory: %s\n", err)
		os.Exit(1)
	} else if !packInfo.IsDir() {
		log.Println("datapacks is not a directory")
		os.Exit(1)
	}

	http.HandleFunc("POST /api/datapacks/{datapack}", func(w http.ResponseWriter, r *http.Request) {
		name := r.PathValue("datapack")

		ok, _ := regexp.MatchString(`^[a-zA-Z_-]+$`, name)
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			log.Printf("datapack name is invalid: %q", name)
			return
		}

		pack, err := os.Create(path.Join(packDir, fmt.Sprintf("%s.zip", name)))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("creating datapack file: %s\n", err)
			return
		}
		defer pack.Close()

		if _, err := io.Copy(pack, r.Body); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("writing datapack file: %s\n", err)
			return
		}

		log.Printf("successfully updated %q datapack\n", name)
		w.WriteHeader(http.StatusOK)
	})

	http.ListenAndServe(addr, nil)
}
