FROM golang:1.23 AS build

WORKDIR /src
COPY . . 
RUN make

FROM scratch

WORKDIR /app
COPY --from=build /src/build/mcos .

ENTRYPOINT ["/app/mcos"]
